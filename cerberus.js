// LOAD LIBS
const Discord = require('discord.js');
//const DBL = require("dblapi.js");
// LOAD SETTINGS
const config = require("./config.json");

const client = new Discord.Client();

// DEFAULT SETTINGS
let prefix = config.prefix;

let role_guild_member = config.default_auto_guild_member;
if (config.auto_members) {
    role_guild_member = config.default_guild_member;
}

client.on('ready', () => {
    client.user.setActivity(`[${prefix}setup] Gère ${client.guilds.size} guildes - http://bot.benftwc.fr`);

    const embed = {
        "title": "BOT READY",
        "color": 8311585,
        "fields": [
            {
                "name": "Nombre de serveurs",
                "value": client.guilds.size
            },
            {
                "name": "Utilisateurs total",
                "value": client.users.size
            },
            {
                "name": "Channels total",
                "value": client.channels.size
            }
        ]
    };

    //client.users.get(config.root_user).send({embed});
    //client.channels.get(config.log_discord_channel).send({embed});
});

client.on("guildCreate", guild => {
    client.user.setActivity(`[${prefix}setup] Gère ${client.guilds.size} guildes - http://bot.benftwc.fr`);

    const embed = {
        "title": `NOUVEAU SERVEUR (${guild.name})`,
        "color": 4886754,
        "fields": [
            {
                "name": "Nom",
                "value": guild.name
            },
            {
                "name": "ID",
                "value": guild.id
            },
            {
                "name": "Utilisateurs",
                "value": guild.memberCount
            }
        ]
    };

    //client.users.get(config.root_user).send({embed});
    //client.channels.get(config.log_discord_channel).send({embed});
});

client.on("guildDelete", guild => {
    client.user.setActivity(`[${prefix}setup] Gère ${client.guilds.size} guildes - http://bot.benftwc.fr`);

    const embed = {
        "title": `SERVEUR SUPPRIMÉ (${guild.name})`,
        "color": 13632027,
        "fields": [
            {
                "name": "Nom",
                "value": guild.name
            },
            {
                "name": "ID",
                "value": guild.id
            },
            {
                "name": "Utilisateurs",
                "value": guild.memberCount
            }
        ]
    };

    //client.users.get(config.root_user).send({embed});
    //client.channels.get(config.log_discord_channel).send({embed});

});

client.on('guildMemberAdd', member => {
    const role = member.guild.roles.find("name", config.role_new_user);
    member.addRole(role).catch(console.error);
});

client.on('message', async msg => {
    // Prevent "botception".
    if (msg.author.bot) return;

    if (msg.channel.name === config.welcome_chan) {
        if(is_owner(msg)) {
            return;
        }
        removeCaller(msg, 50);

        let call = msg.content.toLowerCase();
        if (call.startsWith("ok") || call.startsWith("accept")) {
            console.log(msg.author.name + " accepted");
            msg.member.addRole(msg.guild.roles.find("name", role_guild_member)).catch(console.error);
            msg.member.removeRole(msg.guild.roles.find("name", config.role_new_user)).catch(console.error);
        }
    }

    if (msg.content.indexOf(config.prefix) !== 0) return;

    const args = msg.content.slice(prefix.length).trim().split(/ +/g);
    const command = args.shift();
    if (command) {
        if (command.startsWith('support')) {
            msg.reply(`Toute demande d'aide ou problème passe par un ticket sur le site https://gitlab.com/benftwc/cerberus/issues ou par le discord https://discord.gg/9n38Vav merci :wink:`);
        }

        if (command === 'setup' && is_owner(msg, true)) {

            const owner = msg.guild.members.get(msg.guild.ownerID).user;

            msg.reply(`Le bot va a présent installer divers éléments sur ce discord :
            - Un role \`${config.role_new_user}\` pour les personnes en attente de confirmation du règlement
            - Un role \`${role_guild_member}\` pour les personnes qui ont acceptés le règlement
            - Un channel \`${config.welcome_chan}\` qui servira d'accueil / charte / aide aux nouveaux arrivants

            - La commande \`§purge\` permet de kick les personnes qui n'ont pas acceptés la charte. Utile dans le cas d'events de masse :wink: **nécéssite la permission \`Administrator\`**

            - **Seul le propriétaire du serveur, \`@${owner.username}#${owner.discriminator}\` peut écrire dans le channel de bienvenue (\`#${config.welcome_chan}\`)**
            
            Exemple de message avec du formatage : https://gitlab.com/snippets/1776431

            Pour débuter l'installation, tapper \`${config.prefix}start_setup\`, ensuite, allez dans le salon \`#${config.welcome_chan}\` fraichement crée : Il est déjà opérationnel.

            TIPS : Vos liens d'invitations doivent pointer dessus, il est également recommandé de ne pas ajouter de bot qui DM ou ping lorsqu'un membre rejoint le discord afin de ne pas le perdre.

            Besoin d'aide ? \`§support\` :wink:
            `);
        }

        if (command === 'purge' && has_admin_access(msg) && msg.guild.available) {
            removeCaller(msg, 1);

            let m = msg.guild.roles.get(msg.guild.roles.find('name', config.role_new_user).id).members.map(m => m.user.id);

            if (m.length === 0) {
                msg.reply('Personne a kick');
                return;
            }
            m.forEach(function (userid) {
                msg.guild.members.find('id', userid).kick('AZE');
            })
            msg.reply(`${m.length} utilisateurs kickés (coincés dans l'accueil depuis quelques temps)`);
        }

        if (command === 'save' && has_admin_access(msg)) {
            removeCaller(msg);

            const fs = require("fs")
            fs.writeFile("./config.json", JSON.stringify(config), (err) => console.error);
        }

        if (command === 'start_setup' && has_admin_access(msg) && msg.guild.available) {
            if (msg.guild.roles.find('name', config.role_new_user) === null) {
                console.log('Roles crées');
                await msg.guild.createRole({
                        name: config.role_new_user,
                        hoist: true,
                        mentionable: false,
                        color: "#8e7bfa",
                        deny: ['VIEW_CHANNEL']
                    }
                )
                    .then(
                        msg.guild.createRole({
                                name: role_guild_member,
                                hoist: true,
                                mentionable: false,
                                color: "#c44747",
                                allow: ['VIEW_CHANNEL']
                            }
                        )
                    );

            }

            await msg.guild.createChannel(config.welcome_chan, 'text', [{
                type: 'role',
                id: msg.guild.id,
                deny: ['READ_MESSAGES', 'SEND_MESSAGES']
            },
                {
                    type: 'role',
                    id: '' + msg.guild.roles.find('name', config.role_new_user).id,
                    allow: ['READ_MESSAGES', 'SEND_MESSAGES']
                }])
                .then(channel => console.log(`Created new channel ${channel}`))
                .catch(console.error);
        }
    }

    if (command === 'invite') {
        msg.channel.sendMessage(`Vous pouvez m'inviter chez vous en cliquant sur ce lien https://discordapp.com/api/oauth2/authorize?client_id=489558771832586247&permissions=8&scope=bot`);
    }

    if (command === 'stats') {
        msg.channel.sendMessage(`Manage actuellement ${client.users.size} personnes, dans ${client.channels.size} channels, sur ${client.guilds.size} serveurs.`);
    }
});

client.login(config.token);


function has_admin_access(msg) {
    if (!msg.channel.permissionsFor(msg.member).has("ADMINISTRATOR", false)) {
        msg.channel.sendMessage(`Vous n'avez pas la permission \`Administrator\``);
    }
    return msg.channel.permissionsFor(msg.member).has("ADMINISTRATOR", false);
}

function is_owner(msg, warn = false) {
    const ownerId = msg.guild.ownerID;
    const owner = msg.guild.members.get(msg.guild.ownerID).user;

    if(warn && msg.member.id !== ownerId) {
        msg.reply(`Désolé, seul le propriétaire du serveur, \`@${owner.username}#${owner.discriminator}\` peut utiliser cette commande.`);
    }

    return msg.member.id === ownerId;
}

function removeCaller(msg, timer = 50) {
    msg.delete(timer);
}
